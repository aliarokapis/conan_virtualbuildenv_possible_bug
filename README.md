# VirtualBuildEnv Conan 1.57 vs 2.0-beta8

## Description

VirtualBuildEnv is supposed to add the `runenv_info` of the transitive dependencies of the immediate `build_requires`.
This works properly on 1.57 but does not in 2.0-beta8

## 1.57

```bash
cd a
conan create -pr:b=default . a_lib/0.1@conan/testing
cd b
conan create --build-require -pr:b=default . b_lib/0.1@conan/testing
```

The generated `conanbuildenv..` file should contain `a_lib`'s `TEST` `runenv_info` definition 

## 2.0-beta

```bash
cd a
conan create -pr:b=default --name a_lib --version 0.1 --user conan --channel testing .
cd b
conan create --build-require -pr:b=default --name b_lib --version 0.1 --user conan --channel testing .
```

The generated `conanbuildenv..` file does not contain `a_lib`'s `TEST` `runenv_info` definition 


