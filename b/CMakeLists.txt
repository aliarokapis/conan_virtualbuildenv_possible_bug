cmake_minimum_required(VERSION 3.15)
project(b_lib CXX)




add_library(b_lib src/b_lib.cpp)
target_include_directories(b_lib PUBLIC include)



set_target_properties(b_lib PROPERTIES PUBLIC_HEADER "include/b_lib.h")
install(TARGETS b_lib)
